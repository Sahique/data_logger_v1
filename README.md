Version:
•	data_logger.v1 (version 1)
Objective:
•	Read the CSV file in the "raw_data" folder.
•	Extract all the data from the CSV file and store it in the database (mysql).
•	Move the file to the "done" folder or "error" folder based on valid file name.
File Structure:
•	Has three folders "done", "error" and "raw_data".
•	Has two JSON files "config.json" and "mapping_table.json".
•	Has one main script "data_logger_python.py".
•	has one batch file "data_logger.bat".
Database Table Columns:	
date_time	varchar(50)
logID	varchar(45)
logType	varchar(45)
project	varchar(45)
location	varchar(45)
position	varchar(45)
longitude	varchar(45)
latitude	varchar(45)
parammOut	varchar(45)
value	varchar(45)
unit	varchar(45)

Usage:
•	Assuming all the dependencies are installed (python libraries) and the database is set up. 
•	Make sure to give right path for the folders and database to be accessed in the "config.json" file. 
•	All the CSV file that has to be processed to extract the data has to be stored in the "raw _data " folder.
•	Start the main script "data_logger_python.py" .
•	The CSV file is read only if the filename is in the right format (eg:" 20188481_Auenreaktivierung_Auenbecken_AP1.csv")
•	Data from this file will be extracted and stored in database, and the file is moved to the  "done" folder, while the other files will be sent to "error" folder.
