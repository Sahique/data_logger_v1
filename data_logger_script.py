# import OS module
import os
import re
import shutil
import mysql.connector
import datetime
import csv
import json

def read_config_file():
    conf_f = open("C:/Users/Lenovo/PycharmProjects/Bert/config.json", );
    data = json.load(conf_f)
    raw_path = data['raw_path']; error_path = data['error_path']
    done_path = data['done_path']; map_json_path = data['mapping_json_path']

    DB_host = data['DB_host']; DB_user = data['DB_user'];DB_password = data['DB_password']
    DB_database = data['DB_database'];DB_plugin = data['DB_auth_plugin']

    print(raw_path,error_path,done_path,map_json_path)
    print(DB_host,DB_user,DB_password,DB_database,DB_plugin)

def regular_exp_test(file_name):
    p4f=re.compile('[0-9]+-[A-Za-z]+-[A-Za-z]+-[A-za-z0-9]+.csv');    x=p4f.match(file_name); #print("x:",x)
    return x

def demo_DB_write(f_values):
    print("f_values:",f_values)
    # Read from CSV file, log data from file name and deposite into DB
    write_mydb = mysql.connector.connect(host=DB_host, user=DB_user, password=DB_password, database=DB_database,auth_plugin=DB_plugin)
    mycursor = write_mydb.cursor()
    for val in f_values:
        sql = "INSERT INTO data_logger2 (date_time, logID, logType, project, location, position, longitude, latitude, parammOut, value, unit) " \
              "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        val =val;   mycursor.execute(sql, val)
        print(mycursor.rowcount, "was inserted.");  write_mydb.commit()

def read_csv(r_file):
    init=0; body=[]
    with open(raw_path + r_file, mode='r')as file:
        csvFile = csv.reader(file)
        for lines in csvFile:
            #print(lines)
            if (init==0):
                if(lines[0]=="number"):
                    header=lines; #print(header)
                    init=1
            else:
                body.append(lines)
    file.close()
    #print ("header",header); print ("body",body)
    return header,body

def create_data_list(file_name):
    #20188481_Auenreaktivierung_Auenbecken_AP1
    final_set=[];
    longitude="";latitude="";
    x=file_name.split('_'); print(x)
    logid=x[0]; Project=x[1];   location=x[2];
    pos=x[3].split('.');    position=pos[0]
    #print(logid,Project,location,position)
    # finding lat,long from the mapping table 2
    f = open(map_json_path, );
    data = json.load(f)
    for i in data['map_tab_2']:
        #print(i)
        if (location==i['location'] and position==i['position']):
            latitude=i['latitude']; print(latitude)
            longitude=i['longitude']; print(longitude)
            break
    #  "logID": "20188481","logType": "light","paraIn":["Temp.", "°C (LGR S/N: 20188481, SEN S/N: 20188481)"],"paramOut": "temperature","unit": "°C"
    f_header,f_body=read_csv(file_name)
    #print("f_h:",f_header);    #print("f_b",f_body)
    count=0
    for para in f_header:
        #print("para1: ", para)
        for j in data['map_tab_1']:
            #print(j)
            if (logid == j['logID']):
                for k in j['paraIn']:
                    #print("comp: "+ para+" - "+k)
                    if (para == k):
                        #print ("para: ",para)
                        f_logType = j['logType'];
                        #print(f_logType)
                        f_paramOut = j['paramOut'];
                        #print(f_paramOut)
                        f_unit = j['unit'];
                        #print(f_unit)
                        for  m in f_body:
                            final_val = (m[1], logid, f_logType, Project, location, position, longitude, latitude, f_paramOut,m[count],f_unit)
                            final_set.append(final_val)
                        #print("final:", final_set)
        count+=1
    return final_set

#################################################################################################################
# Main Script

raw_path="";    error_path="";  done_path=""; map_json_path=""
DB_host=""; DB_user=""; DB_password=""; DB_database=""; DB_plugin=""

conf_f = open("C:/Users/Lenovo/PycharmProjects/data_logger/config.json", );
data = json.load(conf_f)
raw_path = data['raw_path'];    error_path = data['error_path']
done_path = data['done_path'];  map_json_path = data['mapping_json_path']

DB_host = data['DB_host'];  DB_user = data['DB_user'];  DB_password = data['DB_password']
DB_database = data['DB_database']; DB_plugin = data['DB_auth_plugin']

read_config_file()
# Get the list of all files and directories
print("raw_path:",raw_path)
dir_list = os.listdir(raw_path)
print("Files and directories in '", raw_path, "' :")
print(dir_list)     # prints all files
for f in dir_list:
    out = regular_exp_test(f);  print ("out: ", out)
    try:
        if (out!=None):
            prepared_data=create_data_list(f)
            #print(prepared_data)
            # write into DB FUNCTION
            demo_DB_write(prepared_data)
            shutil.move(raw_path + f,done_path)
        else:
            shutil.move(raw_path + f, error_path)
    except:
        print("exception occured")
