from flask import Flask, render_template,jsonify
import mysql.connector
import json
from flask_cors import CORS
app = Flask(__name__)
CORS(app)

@app.route('/')
def index():
    print("done")
    return 'hello world'

@app.route('/DB/<string:data_str>', methods=["GET"])
def DB_data(data_str):

    DB_host = "";   DB_user = "";   DB_password = "";
    DB_database = "";   DB_plugin = ""
    res=""
    conf_f = open("C:/Users/Lenovo/PycharmProjects/Bert/config.json", );
    data = json.load(conf_f)
    raw_path = data['raw_path'];    error_path = data['error_path']
    done_path = data['done_path'];  map_json_path = data['mapping_json_path']

    DB_host = data['DB_host'];  DB_user = data['DB_user'];
    DB_password = data['DB_password'];  DB_database = data['DB_database'];
    DB_plugin = data['DB_auth_plugin']

    p1 = "";
    header = [];
    data_vals = [];

    try:
        w=data_str.split(';')
        pro=w[0]; id=w[1]; para=w[2]; f_date=w[3]; t_date=w[4];
        # "http://127.0.0.1:5000/DB/Auenreaktivierung;20188481;temperature;22.03.2019 04:00:00;22.03.2019 06:00:00"
        write_mydb = mysql.connector.connect(host=DB_host, user=DB_user, password=DB_password, database=DB_database,auth_plugin=DB_plugin)
        mycursor = write_mydb.cursor()
        para_values = para.split(',')
        print(para_values)
        p1 = "parammOut = '" + para_values[0] + "'"
        if (len(para_values) > 1):
            for val in para_values:
                p1 = p1 + " OR parammOut = '" + val + "'"
        else:
            p1 = "parammOut = '" + para_values[0] + "'"

        x = "SELECT * FROM data_logger.data_logger2 WHERE "
        y = x + "project = '" + pro + "' AND logID = '" + id + "' AND " + p1 + " AND date_time BETWEEN '" + f_date + "' AND '" + t_date + "'"
        print(y)
        mycursor.execute(y)
        myresult = mycursor.fetchall()
        for x in myresult:
            data_vals.append(x);  print(x)

        mycursor.execute("DESCRIBE data_logger.data_logger2 ")
        myresult1 = mycursor.fetchall()
        for x in myresult1:
            header.append(x[0]);  # print(x[0]
        print(data_vals, header)
        response = jsonify({"header":header,"data":data_vals})
        response.headers.add("Access-Control-Allow-Origin", "*")
        response.headers.add("Access-Control-Allow-Credentials",True)
    except:
        print("error occured")
    return response


if __name__ == "__main__":
    app.debug = True
    app.run()

#   http://127.0.0.1:5000/DB/Auenreaktivierung;20188481;temperature;22.03.2019%2004:00:00;22.03.2019%2006:00:00
